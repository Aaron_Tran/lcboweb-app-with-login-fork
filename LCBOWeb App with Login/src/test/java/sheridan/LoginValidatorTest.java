package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

    @Test
    public void testIsValidAlphanumLoginRegular( ) {
        assertTrue("Invalid login" , LoginValidator.isValidLoginName("Aaron123"));
    }

    @Test
    public void testIsValidAlphanumException( ) {
        assertFalse("Invalid login" , LoginValidator.isValidLoginName(""));
    }

    @Test
    public void testIsValidAlphanumBoundaryIn( ) {
        assertTrue("Invalid login" , LoginValidator.isValidLoginName("aaron12"));
    }

    @Test
    public void testIsValidAlphanumBoundaryOut( ) {
        assertFalse("Invalid login" , LoginValidator.isValidLoginName("@"));
    }

    @Test
    public void testIsValidLoginLengthRegular( ) {
        assertTrue("Invalid login" , LoginValidator.isValidLoginName("aaron1234"));
    }

    @Test
    public void testIsValidLoginLengthException( ) {
        assertFalse("Invalid login" , LoginValidator.isValidLoginName("a"));
    }

    @Test
    public void testIsValidLoginLengthBoundaryIn( ) {
        assertTrue("Invalid login" , LoginValidator.isValidLoginName("aaron1"));
    }

    @Test
    public void testIsValidLoginLengthBoundaryOut( ) {
        assertFalse("Invalid login" , LoginValidator.isValidLoginName("aaron"));
    }
}