package sheridan;

public class LoginValidator {
	public static boolean isValidLoginName( String loginName ) {
		int count = 0;
		
		for (int i = 0; i < loginName.trim().length(); i++) {
			if(!(Character.isDigit(loginName.charAt(i)) || 
					Character.isLetter(loginName.charAt(i)))) {
				return false;
			} else {
				count++;
			}
		}
		
		if (count < 6) {
			return false;
		}
		
		return true;
    }
}
